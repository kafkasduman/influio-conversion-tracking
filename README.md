# Influio | Gtm Dönüşüm Takip Şablonu






## Açıklamalar

Influio GTM Dönüşüm Takip Şablonu, Influio entegrasyonunun google tag manager aracılığıyla tamamlanmasına yardımcı olur.
Bu şablon, Influio tarafından yönlendirilen trafiği takip eder ve satışa dönüş gerçekleştiğinde gerekli bilgileri Influio'ya iletir.
Şablon tetiklendiğinde, Influio takip sunucusuna dönüşüm detayları iletilir. (Sipariş numarası, sipariş tutarı vb..)

Dönüşüm takibinin yapılabilmesi için `Influio Conversion Tracking`  ve `Influio Transaction Id Handler` şablonlarının her ikisini de yüklemelisiniz.

Template (.tpl) linki;


[Influio Conversion Tracking](https://drive.google.com/file/d/1qmditnevR5VLT53X5UoufLypbxbk8Elg/view?usp=sharing)

## Kurulum

1. `Influio Conversion Tracking.tpl` dosyasını [indirin](https://drive.google.com/file/d/1qmditnevR5VLT53X5UoufLypbxbk8Elg/view?usp=sharing).

2. Şablonlar sayfasından, yeni bir etiket şablonu oluşturun.

    ![tag_template](./img/tag_template.png)

3. `Influio Conversion Tracking.tpl` şablonunu içeri aktarın

    ![import_template](./img/import_template.png)


## Yapılandırma


Etiketler Sayfasından `Influio Conversion Tracking`'i kullanarak yeni bir etiket oluşturun.

![create_tag](./img/conversion_create_tag.png)

Influio Dönüşüm Takip Şablonu **Teşekkürler / Sipariş Tamamlandı** sayfasında trigger edilmelidir.


Takip şablonunda istenen bilgileri dataLayer değişkenlerinize göre yapılandırın.

Şablonda istenen `Campaign Id`, `Order Id` ve `Amount` alanlarını doldurun.

![create_tag](./img/create_tag.png)

`Campaign Id` : Entegrasyon esnasında tarafınıza iletilecektir.

`Order Id` : Sipariş kodu. dataLayer değişkenlerini kullanarak yapılandırın.

`Amount` : Kargo ücretleri düşülmüş Sipariş tutarı. dataLayer değişkenlerini kullanarak yapılandırın.

Order Id ve Amount bilgileri için tanımlı dataLayer parametreleriniz yoksa aşağıdaki örnek kodu inceleyebilirsiniz.

### Veri Katmanı değişkenleri

dataLayer katmanına veri iletilmesi (kod demeti örnek olarak paylaşılmıştır, web sayfanızda değişiklik gösterebilir):

```javascript

window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'orderId': 'SKU131425',  // sipariş numarası
  'amount': 28.89 // kargo ücreti düşülmüş toplam sipariş tutarı.
});
```

### Google Tag Manager değişkenler

Değişkenler menüsünden, dataLayer parametrelerinizi tanımlayabilirsiniz.

![variables](./img/amount_var.png)


![variables](./img/orderId_var.png)

 dataLayer isim ve değişkenleri:

```md
'orderId' -> {{orderId}}
'amount' -> {{amount}}
```

### Notlar

Kampanya kodunuz (`campaign_id`) entegrasyon esnasında tarafınıza iletilecektir.

Yapılandırılmış şablon örneği

![template_done](./img/template_done.png)
