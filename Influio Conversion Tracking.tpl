﻿___INFO___

{
  "type": "TAG",
  "id": "cvt_temp_public_id",
  "version": 1,
  "securityGroups": [],
  "displayName": "Influio Conversion Tracking Dev",
  "brand": {
    "id": "brand_dummy",
    "displayName": "",
    "thumbnail": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAAD0CAYAAACo2tvDAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJd2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDggNzkuMTY0MDM2LCAyMDE5LzA4LzEzLTAxOjA2OjU3ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNCBNYWNpbnRvc2giIHhtcDpDcmVhdGVEYXRlPSIyMDE5LTEwLTA3VDEwOjQ5OjIyKzAyOjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDIwLTExLTE5VDAxOjEyOjI4KzAzOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAyMC0xMS0xOVQwMToxMjoyOCswMzowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6YzRiZThhYjEtMDE4NS00OTg5LTkzYWQtZGE0MzBiMjI2MzA5IiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MmU5M2RkNzMtNjVjOS00NDQ4LTk3ODItYjBlZTNiZTA0MzE5IiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6Rjc3RjExNzQwNzIwNjgxMTkxMDlBRkFGNEU1RUU5NjgiIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSI+IDx4bXBNTTpIaXN0b3J5PiA8cmRmOlNlcT4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImNyZWF0ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6Rjc3RjExNzQwNzIwNjgxMTkxMDlBRkFGNEU1RUU5NjgiIHN0RXZ0OndoZW49IjIwMTktMTAtMDdUMTA6NDk6MjIrMDI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDUzQgTWFjaW50b3NoIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo4NWM2YjZiMC1jMjkxLTQzYWItYjIzNS1iNGVjZTg0YTJlOTEiIHN0RXZ0OndoZW49IjIwMTktMTAtMDhUMTA6NTE6MTQrMDM6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChNYWNpbnRvc2gpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjb252ZXJ0ZWQiIHN0RXZ0OnBhcmFtZXRlcnM9ImZyb20gYXBwbGljYXRpb24vdm5kLmFkb2JlLnBob3Rvc2hvcCB0byBpbWFnZS9wbmciLz4gPHJkZjpsaSBzdEV2dDphY3Rpb249ImRlcml2ZWQiIHN0RXZ0OnBhcmFtZXRlcnM9ImNvbnZlcnRlZCBmcm9tIGFwcGxpY2F0aW9uL3ZuZC5hZG9iZS5waG90b3Nob3AgdG8gaW1hZ2UvcG5nIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpkYWQ5ZWY1Yy1kMWZhLTQxZGItYTViYy0wYzE1ZGEwYWFiNTgiIHN0RXZ0OndoZW49IjIwMTktMTAtMDhUMTA6NTE6MTQrMDM6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChNYWNpbnRvc2gpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpjNGJlOGFiMS0wMTg1LTQ5ODktOTNhZC1kYTQzMGIyMjYzMDkiIHN0RXZ0OndoZW49IjIwMjAtMTEtMTlUMDE6MTI6MjgrMDM6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCAyMS4wIChNYWNpbnRvc2gpIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4NWM2YjZiMC1jMjkxLTQzYWItYjIzNS1iNGVjZTg0YTJlOTEiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6Rjc3RjExNzQwNzIwNjgxMTkxMDlBRkFGNEU1RUU5NjgiIHN0UmVmOm9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpGNzdGMTE3NDA3MjA2ODExOTEwOUFGQUY0RTVFRTk2OCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoJ30q8AABtLSURBVHic7Z15fJxVuce/k6ZNFwql+0bJAMWyVcwU2oTlisgii8uHxXtFVBR3vXVDweXei9tF1Is7CnLZRFAR8VqVTRaxmSImiECBgiSUUrGlTUtbumfuH78ZmqSzZ95zzrx5vp/PfKCTyXueyczvPec851kSmUwGwzDiRYNvAwzDqD0mbMOIISZsw4ghJmzDiCEmbMOIISZsw4ghJmzDiCEmbMOIISZsw4ghJmzDiCEmbMOIISZsw4ghJmzDiCEmbMOIISZsw4ghJmzDiCEmbMOIIY2+DciRTqbKeVkj0ATMBY4BDgMOAF4FDMvz+l5gBfAM8CBwF7AU2AJs6/vC1q6OKi03jPAIRthl8gbgFOBwYBowHhhb4nfGAbOBFuBUYDlwO3ADsDUiOw3DK/Ug7InAe4E3A/sDE6q4RhMwM/tYgG4OnwJ+D1wK/LMWhhpGKCRCKWaYZynehAT9KTQ7j4hg2F5gFXA1cFlrV8fqCMYwDOeEKOwGYB5wOdpLu1hV9AJPAxcCi1q7OrY7GNMwIiM0r3gT8CFgCdoTu9oqNAAHArcAX08nU+McjWsYkRCSsKcCXwK+CyQ82rEQuDadTE33aINhDIogluLpZGomcCVwsm9b+nAHcF5rV8dK34YYRqV4n7HTydS+wBWEJWqAE4Fr0snUNN+GGEaleJ2x08nUCOAq4O3ejCjN3cC5NnMb9YTvGfsC4GzPNpTidcBl6WRquG9DDKNcvAk7nUydAVxENOfTteZs4BO+jTCMcvGyFE8nU4cCN6MY73phPXBya1fHEt+GGEYpnM/Y6WRqGHAO9SVqgL2AS2xJbtQDPpbircCnPYxbC44EzvNthGGUwqmw08nUGOB81+PWkFHA+9LJVJNvQwyjGK4Fti/he8FLMRs4y7cRhlEMZ8LO7q1PRLNePbMnJmwjcFzO2COBcx2OFyVz0snUAelkql63FEbMcfnFbEaVT+JAM1p91MMZvDEEcSnslOPxomQEMMe3EYZRCJdCe7XDsVywP/FZgRgxw6WwD3c4lgsmAgf5NsIw8uFS2EmHY7lgHLCPbyMMIx8uhT3T4VguGI1mbcMIjsiFnUgkSCR8VjqKjMnAQTF9b0adExcvtQ8yqLqpYQSHCbt6GqiPhgvGEMSlsPP11qpnNmIdRIxAcSnsHodjuWAt0OXbCMPIh0thdzscywUbgRd8G2EY+XAp7MccjuWCHuBZ30YYRj5cCvshh2O54Fmg3bcRhpEPl8Je7HCsqMkAy4BNvg0xjHy4FPYzwD8cjhclK4D7AOvKaQSJS2FvBG51OF6UPAs80NbducO3IYaRD2fCbuvu3Ar8DKh3MWwH7jJRGyHjOvLsYeABx2PWmhXAD30bYRjFcC3sl1BnzXrmjrbuTos4M4LGqbDbujt7gV8Bd7oct4asBL7s2wjDKIXzJJC27s4NwDeozxDTL7R1d67wbYRhlMJXdtfdwOWexq6WnwPX+jbCMMoh8m6bhQoRtDe3jEJe8tMjNaA2PAOc1Nbd+XS+H/roWGoYxfAmbID25pZXAT8FWiI1YnBsBt7Q1t15X6EX1OJvmE6mBn0NzzSg1NyGPo8EKiG1JyrZ3IRy2Heiv+tOYCtyqm5BEX1kn99JHRWyaO3q8G1CP7wWCmjr7nyyvbnls8CVhFkYcD3wjmKiHqI0IMEOz/7/fsDBwIHALGAC6tOWQKKeUeJ6y9Ax4svANuBvwHJ0PPpc9rmt2Yctj8rA64ydo7255QzgMsIS94vA+W3dnb8u9cIhMmM3AXugAo5zgTOAA5C450Y05kY0cz8CpLOPJ4DVwAYk9CAIbcYOQtgA7c0tJwCXEkb98eXAwrbuzlvLeXGMhT0aiXk6cDJwPGqU4LOU9CpgKXAbcAeqYtODlvbFSKDVxc4ojApN2CHV7LoTeCvwTeA0j3Y8CFwwxJff05CAT0KtmeYD471atIvJ2cdrgQuAx4H7gV+jG3KxRKMhs4wPSdigvda/ARcCHwPGOB7/GuC/27o7lzkeNwQaUbPBQ4H3AscRfsvjCcDR2cfb0ZL9ZyhP/jn6L9UzmLC9shH4PLoLfxQ41cGYf0bbgNvaujuHWo71MLRXPg54BzAP7ZvrjX2yj+NRUY9bgUVo2T5kBJ0jRGHnuB19QCcB5wPHRjDGo8hp94e27s56LXPUSPUZc3OBo9BsdwiwV62M8kgTsADdoM5CgUW3I4EPmfz5kIUNcpRcjz6YOWgP/ha0B6yWbcBvgKvQscrzg7SxHpkEnAJ8BO2l9/ZrTiQ0Iv9AC3Am8F3kVc8bZBQ3Qhd2jlXZxwPAf6CWvMcAh6Fl5IHkfy+9aK+1DO27lqB92CbiU9ao0tn6dLQCOoZ4CnogCeAIlLzzFPB95FEv5UWva4I57qpn6uS463DgHOBfiV+DxEpYjlaAP0a+lZpgx12Ga4YB7wTej2au+N9pizMLef3nA98GbkIRb7HChB1v5gHnoTPfg/2aEhxzUczEPOA7KKItNkQu7PbmltOQ8+KLbd2dz0Q9nivam1tmA19Be7YQg1lOAj6Agn2G+ixdiHHI3zAD+BoxqhPvIh+7ES0Fn2hvbvlxe3PL4Q7GjIz25pZD2ptbrkQRT2cRnmiGARehElSnor9/3Boi1pLh6O90PfBJlIVW97hcig8H3gO8p725ZQk6brqlrbtzrUMbqiKbO34q8EHgdZ7NKcYo4D/RLD3Lsy31xDCUoXYWMBXN3i96tWiQ+NpjL8g+Lm9vbrkXuAW4s1AhAx+0N7fshfamZwJvRskQITMGuA4dZ9kMXR3zUJjqDGAhyiKrS3w7zxqB12cftDe3PA/ci9oB/RX4m6sQz/bmlhnAa4BWFJY4j/oRyHRUaup0wtsa1BPDUMBOL/KdfATFT9QdvoU9kBnorPWc7L8z7c0tT6H97DOoFW+uVdBqYE25wm9vbhmN7saTUUzxfuhDnI08pFNq9i7cMhNta070bUhMSKCAp9HAjcC7qcOuqqEJeyC5P/KBhV7Q3tyyFZXVWZd9aiuKCc4tnfdAy9SRkVnphwZ0U7ocJXAYtWVm9vET4GzqrO9c6MIuh6bsIw4JDOXQgJaKc4DPID+AER1Hoyi1D6Dw5LrAV/lho3p60bbhfOBcbE/tgkkoDbRuThpM2PXHeHTe+nFM1K44Ah2DfRn5aILHhF1fNKGz9Nciv4LhjukoLPci9DkETRz22EOJU4B3oVRVwz0p5FBbgkowBYvN2PVDM/ApTNS+mYJm7UN8G1IME3Z9MBG4GGjzbYgBqNDHV5BTDQLUUXAGGbvRhFIv3+bbEKMfp6LkpiAxYYdPC4pbNn9IWDQCnwA+TIA9xkzYYTMbiboujliGINPQKcVsgHQy1ZhOpoLILzBhh8sItKc+CvucQmZv4Oh0MjUKzdxBzN4ulndDrlh7jdgP+BAKjAhiFjDyMhlFAT7Y2tXxqG9jcti+LUxGozzwefifrZehNNr7Uenmlwb8fCQ6gjsKlTQ+YhBjZVA03V0oi++v2ceaAa/bEx03nQy8Ef29fNGIKsC+NZ1MrWjt6ljn0ZZXsBk7TF6Nqs34EvU2VInlOpQttyX7KNSp8hHg90jkU1EQx+eprHvLGtSV5XrUl3x71o5CddM7gV+iHm/TUDeT8/DTPHA0ao90N3CPh/F3w8UXZ0g1Q6sB41F53GYPY69kl1C+lf33GtRcoVj72QwqwN+DcufvRGWG3oyaPBT6/DOoG8tbkQPqUlT3ez0qCVysGUIv6vP2TzSrfxa19z0X+GMJe6NgFvC+dDI10fG4efG9zDP6k0DL2ij6lBUig2bj/0WdVb4DrGXwseirUGvb16ObxfPsciz1AitQhZJW4BfopjCY3lrb0DbhBrSN+R7uu70cBRzpeMy82IwdFhPQ8db+jsbbhmbU09HSfy21/6w2opvFv6AGeT3AT1FRyCvRUr+W38MMqq7zMeANaPZ29f3bB3hHOpny3kvc1Yxtwi6Pw3AbYXYjOoe9y8FYf0dbjGNQzPtTaIbeSXTL5vuBN6F2uq46bR4HHJROprym1NqMHQ6j0JfQBRnkdb4R7U9dsQl1uxzo5Y6SdWjffQPuvoenIM+9N2yPHQ6TUD1wF9yG9vG3OxovRwYtvavt510NCeSM+wRws4PxJqMCiM0OxiqICTsA0slUAxLadAfDPc0uZ9ZA4laRJcGu99SDGhO6aMc0FTg2nUx5ixMxYYdBE2pvOyricXqQJ3pZgZ/HUdh9Qzx7gM9l/xs1b8RjgU1znoXBAbjJtb4KNWQoRBBxzjUk3/t5EJ2XR70dmAcc5suJ5krYcZsJakZ2uXYCSiaIkg50trs14nFCZxuqxR71knwcytn2Us/eluIeyd7NhxG9N3w18HXqsKNFRKxHN7moe3Mdi5pVOMeFsPs6MIw+tHZ1ZNA+LOqAlFwst7GLO1HwSpTsg6d9ti3F/XMMWrZFxVYk6oFZWUOdTUjcUTIOaPVRfMHVjG3kIXvMdRLRLtf+RuClcj2yiOLOxMEyCoW1Rn3asRs2Y3uktaujFyVBRMlj7N5zqgH7TEBn+emIx/DSjtlmbI+kk6lmol2Gb0LHOwPpxY4gc+Ri1qNiCh56fpnzzBPZZfg8ou3L/TRuEjzqmfvIf/OrFXsCC7KftzPsuMsfDajvd5RhhyvQjGQUpgudGkRFrse70/BSW4r7owGYT7R/n+VEt+RupPD3Z3iR32so8ns+yADdEY9xJDEUtpGfBNF+2BmUshgVOygcgrqdwu9vNNGcAiRQzH013+moO5c6T+E0Yftjb2BshNffjMoTRUkp73q+eOyNwIYIbBlJ8ZtNMTYTrQNtPI5DS638sD/2Y3ClekuRqwE2WEYiz/2I7DV72BVvXkxEw+gv7BHZ6wzPPr+O6uLWRwF7ZP+7E5Vz2px9VMvG7O8X20IMhlnAvshWJ5iw/TGdaI+cdjC4hI/RwKEowOJ0VFL4OVSs4DbgiRLXz4l6ePY6p2SvNRFVULkDBYh0Uv7f4TWoomkKlZFaj0oQ3wosRQKthp0V2FAt04CHIh7jFUzY/phGtFuhBqr/fPcG3gV8g/42TkFHdBcBHwduovhMORyVJbqM3feZbcAHUFO7Wyk++yeA1wLfBw4aYM9FqDrK14BLSthTiGFE7+SdGvH1+2F7bH+MI9ovUyNyJlXze2chwRT6fuwF/BdKNy1EA+rUcSmFnUdTgR+gFUGhZXADugn8gP6i7ksTcCFqGjCiiE3FiFrYUafl9sOE7Y+onSlNFO7S2fdLPPA7MBLV7JpEcWahqqMzstcbeJ29gItRSeViTEFN5HOBOgMFtifqSjKnxHVGAF9FTQMqZU+qvyGUyx4RX78fJmx/jCDaWaIJOLjIzxN9/tvXjgPQ+Xo5nIxSTvNFF05Ae+JyOATt4WH3ve44iq8M+jKxz3UqYRbVrW4qwYRt1IxCe+y+JaH7Oo4akGOqkutn0P64b23wBNme0RUwo8DzlX5HJ1C5dztqUYNjrZmw/eHib79HFeNUeg5c6PqVisXXdzHXSDBqTNhDBBehtklgbpmvbUCifryC668k/9lspsLrgJrrQf+/yzB0dr6uguusobJgk0Mpf8swGJyGVpuw/eEibXI/VNa4HHIz9VPobLkcfokyyPLxD+DPZV7nHnaVRO77d8mFxS4q8zpdVB73nUJ+hahxmiZrwvaHizavI5Bjqpzz7JxXeC3wRXYvzjCQTuBqCp8bb0Tny8UKKPYCLwBfYteMPfDnm9CR2aMl7HketdKtpGBjI5qtXcRzuOx+YsL2iKuqGhNQMEwpcl+8DPA7dI5diC3oiOrhIq/pBRaj8+UXCrxmNfA+infEzKC0yoVo6V+I76AVRCUCGo+7IghOa7Zb5Jk/luHmw25B/aIvK/G6vrZsRy1vH0IRX2ei5WoXqp/2B9TUrxz7bwaeBI5GUWst6L0vAv6CZuJylqn3oDJS81HU22i0urgdhbhWUgkl5084Cb0/F6xwNA5gwvbJGtwIuwkJcxGVFV3YjuKvl6F+1iOyz22gsllxB7pBPII80E3ZxwYqy/LKoPzy55HIM2g78zJysFVCL7pRvRd3hQZdtBV6BRO2P9bjzqGyADgDxVJXSi4Ta7DsQPvuahM1cuwEXhy8OcwHjqrBdcol6uYE/bA9tj9W4caBBvqcPwgc72i80DkY+CRue9cV8jNEggnbHyuAJQ7Hm4WSO6LKOa4XcrHwLs6uc3SRv21xZJiw/bGe/Ec8UfJ+4G1I3EOxemwjcBqarV2yGh3bOcOE7Y8EbvfZOS5AS/K+8eJDgQTyNXzBw9irsQCVIUMv0VfHzMchqLjB0R7G9smRwEcpP8S2ljyN43NsE7YnWrs6dqKmcD6Ww6eh89+3eBjbB3OBLwNnexr/PqItlrgbJmy/bCT6SqKFGIVKDZ3haXxXHANcA7ze0/gbgK5sy2RnmLD9shIFW/gggUJNr0Ve4jhyJnAjbj3gA3kY+LvrQU3YHmnt6tiMumH6ZAxwFfCZPs/F4XvxThT+WqiAgysex/H+GuLxAdY7zkrSluAS4FvZ/3f+Rawhw4DPoeW37+/3BpTgMpgy0FXh+40bSn+sJNUwShYicY/2bEc1NKKCiN9DjrIQeBJYnHWUOsWE7Z81KFMqFBaipI9SVUFDYyZwOapVHgor0efrHBO2f7YD1/k2YgBvQvvuI30bUiavBq4grOO7nag3+WBaD1WNCdszrV0dvSi/uVTFEte0oX3qqZ7tKMUJwE8ov0SxK3qAu1u7OpyeX+cwYYfBWiTu0DgIlT/6sG9DBpBLNz4duB4VJAyNxajumxdM2GHwMuqD5eXuXoJJqC/WQt+G9GEH8nxfy64OIiHRi1Y763wZYMIOgGxU0kOoVFCIjEHHYV/1bQiq5HIxSuZw2g+rApYCj2W3WV4wYYdDD3AL4WZcjUSZYd/DTeeMgSSAZuCbqBqpDxvKYSdaSSz3aYQJOxy2o0ipB3wbUoRGtN++iuqa3w2GA4H/AT5C2CW9uoE7Wrs6nAel9MWEHRbrgAd9G1EG56AQ1Ffh5juURPv8kI6z8tELdOAvsecVTNjh0IBCEL+P52VcmbwfbR2OJ9rU0/nAr9DZeuh0AZe0dnW8kE6mXNWNz4sJOxxyjpblqMl7PXAwKtIfRZ5zI3Ai8H8oACV0MqjE8xPwSr69N0zY4bEZiWWpb0PKZCw6qnsXtfs+NQLnoWIQk2t0zahZClyXzdgDIJ1MeaspZ8IOk+XAj3Dc72mQXI323YOtgroH8r5fMWiL3LEFnVs/MeB5E7bR70uwDS1BSzWiC42vosyq8WW8Nl+V1AmoJ1gI5+WVkAZubu3qeLnvk3aObeTjOTQLVNIGJwQ+jRrkzaT4sdRAUc9AZ9T/HpFdUbEKuKK1q6PbtyF9MWGHw8Av+k7kjKmXvXZfzkEplPOz/87nIe5lVzDOAcB3UdWTeuNJ1BTwlT11Oplq8Lm/BhN26IxDe8565DS0pF5A8VZGKXQKEPoZdT4eBn6Ix5jwQoQcwTPUyLcfm4y7/s1RcCzwY7RvvnHAzxqQmC9C4q43VqLw2ptyT+QqkfrcW+cwYYfNWOp/VXUI8G3UoOBXKIhjMupN/W5gH3+mVU0Ghf/eTKD14UzY4dKIyuaO9G1IDZgEfAjlT7+EappPpT5rq4EaPVxKgEvwHCbscJmICgh4DU2sMfU4Ow/k7+hIz2lb3EoxYYfLZGBf30YY/fgr8t4v9mxHSUzY4bI3Ogs2wuAB5AD8CYHuq/tS746ZODMWHXcZ/ulAedaXo6jA4DFhh8lIlNEU/MwwBOgF7gXOpU5EDSbsUBmDjoni5DirN3pRYM3V6Ky9rj4L22OHySRU+tfwQwZ1Z7kPlWLaTpgVZAtiM3aYmLD90Qs8D/weVYnZ4tec6rAZO0ymEG4VzjizHUXG/Ra4kOIx7kFjwg6P4dRHKaC4sRkVklyEQmDrxlGWDxN2eIwDZvs2YoixFYWJ3gT8gvqqXJMXE3Z4jEVlfY3oyQCr0X76GuBPxEDUYMIOkT2xUFJXLEX51ItQAEpsMGGHx0zikdEVMtvQLP1DFHyS83wnCLfFUkWYsMOiEQWmjPJtSIzZAXwMeb4HNmaIhajBhB0aY7D9dVQ8geqTXQH8zrMtkWPCDovRqLCfUTteAv6IEjg6CTyPulaYsMNiLLC/byNiwkvA3ail7Z+BfxCjpXYpTNhhMRGVDPLBH1Hk1XTqN5z1JRQm/QdURPEBdJw15DBhh0MTcBxu4vd7UEz0XagayD3AGuRY2hc4C5UNbmXwLXtcsQTdnH6KnGI9fs3xS+TCbuvuvBWPPYxcUKP1XRPRN5NfhTKWfoOOeXrQUU/foIzVqLXQOOAEdLM5DtUrCyl1cR06h16KBH0v8CIKDR3yRC7sTGbIbGsGSxMwp8bXzKCZ+E+ov9SvgWfRkrtYgsMW5GS6Afg5cuodjeqAL0A3oFySioubdib72A48DtyPOnG2Ay+j9xKLiLFaYUvxcBhFbY66dqCZ+B7gERQDvQzNcJXeZXtRHPVWNMv/Fol8X1RKeDZy+E1EpZJHoa1EA7ua7pUj/MyAR06oj6N982PIAfYoujGtx6rLFMWEHQ7TkECqYQewCZ3VppEI/4QirBqonQh6gY1IaI/1eX5v5M2fg47rJmefG4Y6aB7M7iLP3WS6UQPCTUiwm9Dy+rns+/lnjWwfUpiwAyCdTDUCR1TwK73sWk4/BvwFifku+lf6yM2clZJAoix3eduTteEvRV7TiBxxw5DdOfttrxYBJuwwGAEcXuZr16O84TSwFvgR+R1GOUFXUywgQ+33rDsiuKZRABN2GDSi8+OBZND+thc5sxYjD/CDqNNjMXL7VWMIYsIOg152n3VfRvWsF6MjqPuy/zaMkpiww2Ar6jKxFzADeAp5sztQ4IV5gI2KSNg5cxikkylQL+wpyCu8qc+PY5MnHFdau8JaTNmMHRbL2ZUjHOtoPSNaTNieSSdTCXQMNLAqps3QRtXYUtwwYoh1AjGMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGGLCNowYYsI2jBhiwjaMGPL/Ng2IQl7v6LgAAAAASUVORK5CYII\u003d"
  },
  "description": "Influio conversion tracking template for google tag manager",
  "containerContexts": [
    "WEB"
  ]
}


___TEMPLATE_PARAMETERS___

[
  {
    "type": "TEXT",
    "name": "campaign_id",
    "displayName": "Campaign Id",
    "simpleValueType": true
  },
  {
    "type": "TEXT",
    "name": "order_id",
    "displayName": "Order Id",
    "simpleValueType": true
  },
  {
    "type": "TEXT",
    "name": "amount",
    "displayName": "Amount",
    "simpleValueType": true
  },
  {
    "type": "TEXT",
    "name": "key",
    "displayName": "Campaign Key",
    "simpleValueType": true
  }
]


___SANDBOXED_JS_FOR_WEB_TEMPLATE___

// Enter your template code here.
const log = require('logToConsole');
const queryPermission = require('queryPermission');
const setCookie = require('setCookie');
const getCookieValues = require('getCookieValues');
const getQueryParameters = require('getQueryParameters');
const sendPixel = require('sendPixel');
const encodeUriComponent = require('encodeUriComponent');

let domain = 'https://go.clicktr.com';

let ctr_tid;
let ctr_lid;




  //Get _ctrId cookie value created via gtm
  if (queryPermission('get_cookies', 'ctr_tid')) {
    ctr_tid = getCookieValues('ctr_tid');
    if (ctr_tid.length > 0) {
        ctr_tid = ctr_tid[0];
    } else {
        ctr_tid = undefined;
    }
  }  
  if (queryPermission('get_cookies', 'ctr_lid')) {
    ctr_lid = getCookieValues('ctr_lid');
    if (ctr_lid.length > 0) {
        ctr_lid = ctr_lid[0];
    } else {
        ctr_lid = undefined;
    }
  }  
  

  //Manual value filled at google tag manager UI
  var campaign_id = data.campaign_id;

  //Datalayer values which will be push by merchant.
  var amount = data.amount;
  var orderId = data.order_id;
  var campaign_id = data.campaign_id;

  
  //Will be enchanced
  var params= {
      'amount' : amount || '0',
      'adv_sub5' : orderId || 'null'
  };

  //Callback 
  const processed = (succeeded) => {
  
    if (succeeded) {
      
        const cookie_options = {
        'domain': 'auto',
        'path': '/',
        'max-age': 60 * 60 * 24 * 1,
        'secure': true
      };
     

       if (queryPermission('set_cookies', 'ctr_tid', cookie_options)) {
          log(setCookie('ctr_tid', transaction_id, cookie_options));
       }
      //Success for the send pixel;
        data.gtmOnSuccess();
    } else {
      var url= "http://e.kafkasduman.com/errorHandler";
      data.gtmOnFailure();
    }
  };

  // Transaction_id local source is prioritized
  var transaction_id = ctr_lid || ctr_tid;

  if(transaction_id){
    transaction_id = encodeUriComponent(transaction_id);
    log('transaction id after encodeUri', transaction_id);
    let paramsList = [];
    for (let p in params) {
          paramsList.push(encodeUriComponent(p.toString()) + "=" +           encodeUriComponent(params[p].toString()));
      }
    const url = domain + '/' + campaign_id +'?'+'transaction_id='+transaction_id+'&'+ paramsList.join("&");   
    log('url =', url);
    //sendPixel(url, ()=> processed(true), processed(false));
    
    sendPixel(url, 
              // Success
              ()=> {
              //Set cookie "" | Close session
               const cookie_options = {
              'domain': 'auto',
              'path': '/',
              'max-age': 60 * 60 * 24 * 1,
              'secure': true
              };

             if (queryPermission('set_cookies', 'ctr_tid', cookie_options)) {
              setCookie('ctr_tid', "",  cookie_options);
             }
            //SendPixel Success end
            data.gtmOnSuccess();
          },
              //Error
              (transaction_id,paramsList)=> {
            //Ping error logger
            var failDomain= "https://e.kafkasduman.com/errorHandler";
            var failUrl = failDomain +'/' + campaign_id +'?'+'transaction_id='+transaction_id+'&'+             paramsList.join("&");
            sendPixel(failUrl);
            data.gtmOnFailure();
         }
              
       );
  }else{
  log('Transaction id not exist');
}


data.gtmOnSuccess();


___WEB_PERMISSIONS___

[
  {
    "instance": {
      "key": {
        "publicId": "logging",
        "versionId": "1"
      },
      "param": [
        {
          "key": "environments",
          "value": {
            "type": 1,
            "string": "debug"
          }
        }
      ]
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "get_cookies",
        "versionId": "1"
      },
      "param": [
        {
          "key": "cookieAccess",
          "value": {
            "type": 1,
            "string": "specific"
          }
        },
        {
          "key": "cookieNames",
          "value": {
            "type": 2,
            "listItem": [
              {
                "type": 1,
                "string": "ctr_tid"
              },
              {
                "type": 1,
                "string": "ctr_lid"
              }
            ]
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "send_pixel",
        "versionId": "1"
      },
      "param": [
        {
          "key": "allowedUrls",
          "value": {
            "type": 1,
            "string": "specific"
          }
        },
        {
          "key": "urls",
          "value": {
            "type": 2,
            "listItem": [
              {
                "type": 1,
                "string": "https://go.clicktr.com/*"
              }
            ]
          }
        }
      ]
    },
    "clientAnnotations": {
      "isEditedByUser": true
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "set_cookies",
        "versionId": "1"
      },
      "param": []
    },
    "isRequired": true
  },
  {
    "instance": {
      "key": {
        "publicId": "get_url",
        "versionId": "1"
      },
      "param": [
        {
          "key": "urlParts",
          "value": {
            "type": 1,
            "string": "any"
          }
        },
        {
          "key": "queriesAllowed",
          "value": {
            "type": 1,
            "string": "any"
          }
        }
      ]
    },
    "isRequired": true
  }
]


___TESTS___

scenarios:
- name: Untitled test 1
  code: |-
    const mockData = {
      // Mocked field values
    };

    // Call runCode to run the template's code.
    runCode(mockData);

    // Verify that the tag finished successfully.
    assertApi('gtmOnSuccess').wasCalled();


___NOTES___

Created on 11/19/2020, 2:31:26 PM


